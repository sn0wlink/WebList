<!--
    
    Name: Web List
    Description: List webdirectories for development servers
    Author: David Collins-Cubitt

-->


<html>
<head>
<title>Web View</title>
</head>

<style>

div.header {
    text-align: center;
    color: #ffffff;
    font-size: 20pt;
    font-style: bold;
    font-family: sans, arial, helvetica;
}

body {
    background-color: #393f44;
}

a.dirlist {
    width: 400px;
    text-align: center;
    margin: 20 auto;
    background-color: #262f3a;
    padding:10px;
    border-radius: 200px;
    border-color: #4396fb;
    border-width: 1px;
    border-style: solid;
    padding: 10px;
    font-style: none;
    text-decoration: none;
    font-family: sans, arial, helvetica;
    color: #ffffff;
    font-weight: bold;
    display: block;

}

</style>

<body>
    <div class ='header'>
        Web View - Projects
    </div>

    <?php
    printdir();
    ?>

</body>

</html>

<?php

// List Directory Function
function printdir() {
    foreach (glob("*", GLOB_ONLYDIR) as $file) {
        echo "
        <a class='dirlist' href='$file'>
            $file
        </a>
        ";
    }
}

?>