# Web List
A simple flatfile web project veiwer. Designed for use on the root of a localhost setup.

### Usage
Just drag the index.php into the root folder of the development server and your good to go.

### Screenshot
![image](https://gitlab.com/sn0wlink/WebList/-/raw/master/screenshot.png)
